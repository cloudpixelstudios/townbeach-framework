import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable, BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent {

  //Local logs subject that exposes an subscribable observable
  private logsSubject = new BehaviorSubject(null);
  public logs = this.logsSubject.asObservable();

  constructor(private _data: DataService) {
    let that = this;
    that._data.logs.subscribe(logs => {
      //If logs exist - expose to dom
      if(logs){
        that.logsSubject.next(logs.reverse())
      }
    })
  }

  // Nice date format
  formatDate(dateString) {
    let date = new Date(dateString);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    let minutesString = minutes < 10 ? '0'+ minutes : minutes;
    var strTime = hours + ':' + minutesString + ' ' + ampm;
    return (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  }

}
