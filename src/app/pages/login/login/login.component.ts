import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public credentials = {username : "", password: ""}
  public loggingIn = false;

  constructor(private _data: DataService, private _router: Router, private _messageService: MessageService) { }

  ngOnInit() {
  }

  login() {
    let that = this;
    that.loggingIn = true;
    that._data.login(that.credentials).then(user => {
      that._data.recordLog("info", "User logged in").then(() => {
        that.loggingIn = false;
        that._router.navigate(["games/show-all-games"])
      });
    }).catch(error => {
      that._data.recordLog("error", "Login has failed: " + error.message).then(() => {
        that.loggingIn = false;
        that._messageService.add({key:"primary", severity:'error', summary: "Login Error", detail: error.message});
      })
    })
  }

}
