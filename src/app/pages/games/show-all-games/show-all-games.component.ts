import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { MessageService } from 'primeng/api';
import { Observable, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-show-all-games',
  templateUrl: './show-all-games.component.html',
  styleUrls: ['./show-all-games.component.scss']
})
export class ShowAllGamesComponent {

  // Entire eason array, populates from cloud data
  public season = [];

  //Page loading boolean
  public loading = true;

  // Save config variables
  public changedTimes = false; //Save config watcher boolean
  public saving = false; //Is saving Boolean

  private selectionList;

  constructor(public _data: DataService, public _messageService: MessageService) {
    let that = this;
    that.resetSelectionList();
    //Get full season and push to local 'season' variable only once, ignore updates to cloud data
    that._data.seasons.pipe(take(1)).subscribe(data => {
      that.season = data;
      //Loading complete
      that.loading = false;

      console.log(that.season);
    })
  }

  //Call to save any config changes
  saveChanged() {
    let that = this;
    //Class saving variable changed so spinner activates in dom
    that.saving = true;
    //Pass to data manipulator the season object to save
    that._data.saveAdjustedSeason(this.season).then(() => {
      // Save a log copy of task and inform user of success - reset spinners and UI
      that._data.recordLog("success", "Game changes successfully saved").then(() => {
        that._messageService.add({key:"primary", severity:'success', summary: "Config Saved", detail: "Season change has been saved successfully!"});
        that.saving = false;
        that.changedTimes = false;
      })
    }).catch(error => {
      // Save a log copy of task and inform user of error - reset spinners and UI
      that._messageService.add({key:"primary", severity:'error', summary: "Whoops", detail: "Something went wrong!"});
      that.saving = false;
      that.changedTimes = false;
    });
  }

  //Check if todays date is between two given dates
  betweenDates(start, finish, start2) {
    let today = new Date();
    //Convert AU dates to US format
    let usDateStart = this._data.convertDate(start);
    let usDateFinish = this._data.convertDate(finish);
    let dateStart = new Date(usDateStart);
    let dateFinish = new Date(usDateFinish);

    //Check if today lies between them
    if(today <= dateFinish && today >= dateStart) {
      return true;
    } else {
      return false;
    }
  }

  //Any edit makes the save button appear
  // TODO: Only show if changed, Don't show if changes undone by user.
  changeTimes() {
    this.changedTimes = true;
  }

  //Check if string contains the word "empty"
  isEmpty(string) {
    if(string.includes("empty")) {
      return true;
    }else{
      return false;
    }
  }

  //Add a day to an australian formatted date string, eg. '23/08/2019'
  addDay(currentStartDate, days) {
    //Convert AU date to US
    let usDate = this._data.convertDate(currentStartDate);
    let currentDateObject = new Date(usDate);
    let currentDate = currentDateObject.getDate();
    //Add x days to date
    let pushDate = new Date(currentDateObject.setDate(currentDate + days));
    //Return AU date
    return (pushDate.getDate() < 10 ? '0' : '') + pushDate.getDate() + '/' + (pushDate.getMonth() < 9 ? '0' : '') + (pushDate.getMonth() + 1) + '/' + pushDate.getFullYear();
  }

  //Return a given game object from a day when matched to the round number of that day
  getGame(day, roundNumber) {
    if(day && day[roundNumber]) {
      // Return team 1 and 2 of a given game
      return [day[roundNumber][0].team_name, day[roundNumber][1].team_name]
    } else {
      return undefined;
    }
  }

  //Check if all games for a given day are empty on a given court
  //Variable passed is a day and court. eg. (Monday on Kokomo)
  isEmptyCourt(gameArray) {
    let count = 0;
    //Go through each time slot and check if a game exists
    for(let game in gameArray){
      if(gameArray[game] == "") {
        count += 1;
      }
    }
    //If all 5 games are empty, return false, otherwise return true
    if(count == 5) {
      return false;
    } else {
      return true;
    }
  }

  moveCourt(game, moveTo, comingFrom, seasonIndex) {
    if (game == "000" || game.includes("empty")) {
      this._messageService.add({key:"primary", severity:'info', summary: "Make a selection", detail: "Please select a game to transfer"});
      this.resetSelectionList();
    } else {
      let otherSide = this.season[seasonIndex][moveTo];
      let foundEmpty = false;
      for(let i = 0; i < otherSide.length; i++) {
        if(otherSide[i].includes("empty")) {
          foundEmpty = true;
          this.season[seasonIndex][moveTo][i] = game;
          let oldSide = this.season[seasonIndex][comingFrom];
          for(let x = 0; x < oldSide.length; x++) {
            if(oldSide[x][2] == game[2]) {
              let currentEmpty = 0;
              for(let y = 0; y < oldSide.length; y++) {
                if(oldSide[y].includes("empty")) {
                  currentEmpty += 1;
                }
              }
              let stringVarName = "empty" + (currentEmpty);
              oldSide[x] = stringVarName;
            }
          }
          break;
        }
      }
      this.resetSelectionList();
      this.changeTimes();
      if (!foundEmpty) {
        this._messageService.add({key:"primary", severity:'error', summary: "No spots on other side", detail: "Cannot move game as there are no avaliable spots on the other side!"});
      }
    }
  }

  resetSelectionList() {
    this.selectionList = [
      {
        bondi: "000",
        kokomo: "000"
      },
      {
        bondi: "000",
        kokomo: "000"
      },
      {
        bondi: "000",
        kokomo: "000"
      }
    ]
  }

  setSelectedGame(x, y, z) {
    let that = this;
    switch(x) {
      case "Monday":
        that.selectionList[0][y] = z.value[0];
      case "Tuesday":
        that.selectionList[1][y] = z.value[0];
      case "Wednesday":
        that.selectionList[2][y] = z.value[0];
    }
  }

  //Copy a hidden div to clipboard for posting on social media
  //Takes the full season and the week wanted as arguments
  copyToClipboard(season, index) {
    let that = this;
    //Create invisible dom object
    var newDiv = document.createElement("div");
    //Get static date items
    const mondayDate = season["startDate"];
    const tuesdayDate = this.addDay(mondayDate, 1);
    const wednesdayDate = this.addDay(mondayDate, 2);
    //For each Court/Day in the week
    for(let key in season) {
      if(!(season[key] == "startDate")){
        switch(key) {
          case "mondayBondi":
            //Create a dom object with the day/date title. eg. "🏐🏐🏐 Monday - 26/08/2020 🏐🏐🏐"
            let mbdayTextSpan = document.createElement("div");
            mbdayTextSpan.innerHTML = "🏐🏐🏐 Monday - " + mondayDate + " 🏐🏐🏐";
            let mbtimeArray = [];
            //For any game being played do a string "team1 vs team2"and save to the array. Save empty string where none are playing.
            for (let time in season[key]) {
              if(season[key][time].includes("empty")) {
                mbtimeArray.push("");
              } else {
                mbtimeArray.push(season[key][time][0].team_name + " vs " + season[key][time][1].team_name);
              }
            }
            var mbtimesDiv = document.createElement("div");
            //Check if there are games playing on that court, that day. Add all these games and their times to the hidden dom Object
            if(that.isEmptyCourt(mbtimeArray)) {
              mbtimesDiv.innerHTML = "Bondi<br><b>6:00pm<\/b> " + mbtimeArray[0] + "<br><b>6:40pm<\/b> " + mbtimeArray[1] + "<br><b>7:20pm<\/b> " + mbtimeArray[2] + "<br><b>8:00pm<\/b> " + mbtimeArray[3] + "<br><b>8:40pm<\/b> " + mbtimeArray[4] + "<br>";
              newDiv.appendChild(mbdayTextSpan);
              newDiv.appendChild(mbtimesDiv);
            }
            break;
          case "mondayKokomo":
            let mkdayTextSpan = document.createElement("div");
            let mktimeArray = [];
            //For any game being played do a string "team1 vs team2"and save to the array. Save empty string where none are playing.
            for (let time in season[key]) {
              if(season[key][time].includes("empty")) {
                mktimeArray.push("");
              } else {
                mktimeArray.push(season[key][time][0].team_name + " vs " + season[key][time][1].team_name);
              }
            }
            var mktimesDiv = document.createElement("div");
            //Check if there are games playing on that court, that day. Add all these games and their times to the hidden dom Object
            if(that.isEmptyCourt(mktimeArray)) {
              mktimesDiv.innerHTML = "<br>Kokomo<br><b>6:00pm</b> " + mktimeArray[0] + "<br>" + "<b>6:40pm</b> " + mktimeArray[1] + "<br>" + "<b>7:20pm</b> " + mktimeArray[2] + "<br>" + "<b>8:00pm</b> " + mktimeArray[3] + "<br>" + "<b>8:40pm</b> " + mktimeArray[4] + "<br><br>";
              newDiv.appendChild(mkdayTextSpan);
              newDiv.appendChild(mktimesDiv);
            } else {
              mktimesDiv.innerHTML = "<br>";
              newDiv.appendChild(mktimesDiv);
            }
            break;
          case "tuesdayBondi":
            //Create a dom object with the day/date title. eg. "🏐🏐🏐 Monday - 26/08/2020 🏐🏐🏐"
            let tbdayTextSpan = document.createElement("div");
            tbdayTextSpan.innerHTML = "🏐🏐🏐 Tuseday - " + tuesdayDate + " 🏐🏐🏐";
            let tbtimeArray = [];
            //For any game being played do a string "team1 vs team2"and save to the array. Save empty string where none are playing.
            for (let time in season[key]) {
              if(season[key][time].includes("empty")) {
                tbtimeArray.push("");
              } else {
                tbtimeArray.push(season[key][time][0].team_name + " vs " + season[key][time][1].team_name);
              }
            }
            var tbtimesDiv = document.createElement("div");
            //Check if there are games playing on that court, that day. Add all these games and their times to the hidden dom Object
            if(that.isEmptyCourt(tbtimeArray)) {
              tbtimesDiv.innerHTML = "Bondi<br><b>6:00pm<\/b> " + tbtimeArray[0] + "<br><b>6:40pm<\/b> " + tbtimeArray[1] + "<br><b>7:20pm<\/b> " + tbtimeArray[2] + "<br><b>8:00pm<\/b> " + tbtimeArray[3] + "<br><b>8:40pm<\/b> " + tbtimeArray[4] + "<br>";
              newDiv.appendChild(tbdayTextSpan);
              newDiv.appendChild(tbtimesDiv);
            }
            break;
          case "tuesdayKokomo":
            let tkdayTextSpan = document.createElement("div");
            let tktimeArray = [];
            //For any game being played do a string "team1 vs team2"and save to the array. Save empty string where none are playing.
            for (let time in season[key]) {
              if(season[key][time].includes("empty")) {
                tktimeArray.push("");
              } else {
                tktimeArray.push(season[key][time][0].team_name + " vs " + season[key][time][1].team_name);
              }
            }
            var tktimesDiv = document.createElement("div");
            //Check if there are games playing on that court, that day. Add all these games and their times to the hidden dom Object
            if(that.isEmptyCourt(tktimeArray)) {
              tktimesDiv.innerHTML = "<br>Kokomo<br><b>6:00pm</b> " + tktimeArray[0] + "<br>" + "<b>6:40pm</b> " + tktimeArray[1] + "<br>" + "<b>7:20pm</b> " + tktimeArray[2] + "<br>" + "<b>8:00pm</b> " + tktimeArray[3] + "<br>" + "<b>8:40pm</b> " + tktimeArray[4] + "<br><br>";
              newDiv.appendChild(tkdayTextSpan);
              newDiv.appendChild(tktimesDiv);
            } else {
              tktimesDiv.innerHTML = "<br>";
              newDiv.appendChild(tktimesDiv);
            }
            break;
          case "wednesdayBondi":
            //Create a dom object with the day/date title. eg. "🏐🏐🏐 Monday - 26/08/2020 🏐🏐🏐"
            let wbdayTextSpan = document.createElement("div");
            wbdayTextSpan.innerHTML = "🏐🏐🏐 Wednesday - " + wednesdayDate + " 🏐🏐🏐";
            let wbtimeArray = [];
            //For any game being played do a string "team1 vs team2"and save to the array. Save empty string where none are playing.
            for (let time in season[key]) {
              if(season[key][time].includes("empty")) {
                wbtimeArray.push("");
              } else {
                wbtimeArray.push(season[key][time][0].team_name + " vs " + season[key][time][1].team_name);
              }
            }
            var wbtimesDiv = document.createElement("div");
            //Check if there are games playing on that court, that day. Add all these games and their times to the hidden dom Object
            if(that.isEmptyCourt(wbtimeArray)) {
              wbtimesDiv.innerHTML = "Bondi<br><b>6:00pm<\/b> " + wbtimeArray[0] + "<br><b>6:40pm<\/b> " + wbtimeArray[1] + "<br><b>7:20pm<\/b> " + wbtimeArray[2] + "<br><b>8:00pm<\/b> " + wbtimeArray[3] + "<br><b>8:40pm<\/b> " + wbtimeArray[4] + "<br>";
              newDiv.appendChild(wbdayTextSpan);
              newDiv.appendChild(wbtimesDiv);
            }
            break;
          case "wednesdayKokomo":
            let wkdayTextSpan = document.createElement("div");
            let wktimeArray = [];
            //For any game being played do a string "team1 vs team2"and save to the array. Save empty string where none are playing.
            for (let time in season[key]) {
              if(season[key][time].includes("empty")) {
                wktimeArray.push("");
              } else {
                wktimeArray.push(season[key][time][0].team_name + " vs " + season[key][time][1].team_name);
              }
            }
            var wktimesDiv = document.createElement("div");
            //Check if there are games playing on that court, that day. Add all these games and their times to the hidden dom Object
            if(that.isEmptyCourt(wktimeArray)) {
              wktimesDiv.innerHTML = "<br>Kokomo<br><b>6:00pm</b> " + wktimeArray[0] + "<br>" + "<b>6:40pm</b> " + wktimeArray[1] + "<br>" + "<b>7:20pm</b> " + wktimeArray[2] + "<br>" + "<b>8:00pm</b> " + wktimeArray[3] + "<br>" + "<b>8:40pm</b> " + wktimeArray[4] + "<br><br>";
              newDiv.appendChild(wkdayTextSpan);
              newDiv.appendChild(wktimesDiv);
            } else {
              wktimesDiv.innerHTML = "<br>";
              newDiv.appendChild(wktimesDiv);
            }
            break;
        }
      }
    }
    //Quickly add, copy, and remove from live dom view
    document.getElementById("copy").appendChild(newDiv);
    var selection = window.getSelection();
    var range = document.createRange();
    range.selectNodeContents(document.getElementById("copy"));
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand("Copy");
    document.getElementById("copy").innerHTML = "";
    //Let user know the schedule has been coppied
    that._messageService.add({key:"primary", severity:'info', summary: "Copied to clipboard", detail: "This week has been copied to the clipboard!"});
  }
}
