import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { MessageService } from 'primeng/api';
import { take } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import Tournament from "round-robin-tournament";

@Component({
  selector: 'app-current-season',
  templateUrl: './current-season.component.html',
  styleUrls: ['./current-season.component.scss']
})
export class CurrentSeasonComponent {

  private generating = false;
  public loading = true;
  public teams = {
    monday: {
      kokomo: [],
      bondi: []
    },
    tuesday: {
      kokomo: [],
      bondi: []
    },
    wednesday: {
      kokomo: [],
      bondi: []
    }
  };
  public disabledWeekStart = false;
  public seasonLength = 16;
  private testSeasonSubject = new BehaviorSubject(null);
  public testSeason = this.testSeasonSubject.asObservable();
  public isEdit = false;
  public selectedStartDate;
  public newGamesStartDate;
  public mondays = [];
  public currentWeek = "";
  public riskAccepted = false;

  public createNewSeason = false;


  constructor(public _data: DataService, private messageService: MessageService, private _router: Router) {
    let that = this;

    that._data.seasons.pipe(take(1)).subscribe(season => { that.getSeasonText(season) }) // Set week x of x text
    that._data.teams.pipe(take(1)).subscribe(teams => {
      this.fillTeamLists(teams); // Populate teams list
      this.fillMondaysList(); // Populate list of mondays this year

      if(that._data.action == "rebuild_season") {
        that.generating = true;
        that.isEdit = true;
        that._data.action = "default";
        that.disabledWeekStart = true;

        let today = new Date();
        let monday = this.getMonday(today);
        let nextMonday = this.addDays(monday, 7);

        that.newGamesStartDate = {day: (nextMonday.getDate() < 10 ? '0' : '') + nextMonday.getDate() + '/' + (nextMonday.getMonth() < 9 ? '0' : '') + (nextMonday.getMonth() + 1) + '/' + nextMonday.getFullYear()}
        that._data.seasons.pipe(take(1)).subscribe(seasons => {
          let currentWeek = 0;
          let keepSeason = [];
          for(let i = 0; i < seasons.length; i++) {
            if(i == 0) {
              that.selectedStartDate = {day: seasons[i].startDate};
            }
            if(seasons[i].startDate == that.newGamesStartDate.day) {
              currentWeek = i;
              break;
            }
            keepSeason.push(seasons[i])
          }
          console.log(keepSeason)
          that.seasonLength = that.seasonLength - (currentWeek);
          let mondayKokomo = this.generateSeasonForDay(this.teams.monday.kokomo);
          let tuesdayKokomo = this.generateSeasonForDay(this.teams.tuesday.kokomo);
          let wednesdayKokomo = this.generateSeasonForDay(this.teams.wednesday.kokomo);
          let mondayBondi = this.generateSeasonForDay(this.teams.monday.bondi);
          let tuesdayBondi = this.generateSeasonForDay(this.teams.tuesday.bondi);
          let wednesdayBondi = this.generateSeasonForDay(this.teams.wednesday.bondi);
          if ( keepSeason.length != 0 ) {
            for(let k = (keepSeason.length - 1); k >= 0; k--) {
              if(keepSeason[k].mondayKokomo) {
                if(mondayKokomo == undefined){
                  mondayKokomo = [keepSeason[k].mondayKokomo];
                } else {
                  mondayKokomo.unshift(keepSeason[k].mondayKokomo);
                }
              }
              if(keepSeason[k].mondayBondi) {
                if(mondayBondi == undefined){
                  mondayBondi = [keepSeason[k].mondayBondi];
                } else {
                  mondayBondi.unshift(keepSeason[k].mondayBondi);
                }
              }
              if(keepSeason[k].tuesdayKokomo) {
                if(tuesdayKokomo == undefined){
                  tuesdayKokomo = [keepSeason[k].tuesdayKokomo];
                } else {
                  tuesdayKokomo.unshift(keepSeason[k].tuesdayKokomo);
                }
              }
              if(keepSeason[k].tuesdayBondi) {
                if(tuesdayBondi == undefined){
                  tuesdayBondi = [keepSeason[k].tuesdayBondi];
                } else {
                  tuesdayBondi.unshift(keepSeason[k].tuesdayBondi);
                }
              }
              if(keepSeason[k].wednesdayKokomo) {
                if(wednesdayKokomo == undefined){
                  wednesdayKokomo = [keepSeason[k].wednesdayKokomo];
                } else {
                  wednesdayKokomo.unshift(keepSeason[k].wednesdayKokomo);
                }
              }
              if(keepSeason[k].wednesdayBondi) {
                if(wednesdayBondi == undefined){
                  wednesdayBondi = [keepSeason[k].wednesdayBondi];
                } else {
                  wednesdayBondi.unshift(keepSeason[k].wednesdayBondi);
                }
              }
              if(k == 0) {
                that._data.saveSeason(mondayKokomo, mondayBondi, tuesdayKokomo, tuesdayBondi, wednesdayKokomo, wednesdayBondi, that.seasonLength + currentWeek, that.selectedStartDate).then(() => {
                  that._data.recordLog("success", "Season has been re-made successfully!").then(() => {
                    that.generating = false;
                    that.messageService.add({key:"primary", severity:'success', summary: "Season Re-Generated", detail: "Season has been re-made successfully!"});
                    that.loading = false;
                    that._router.navigate(['games/show-all-games'])
                  })
                });
              }
            }
          } else {
            that._data.saveSeason(mondayKokomo, mondayBondi, tuesdayKokomo, tuesdayBondi, wednesdayKokomo, wednesdayBondi, that.seasonLength, that.selectedStartDate).then(() => {
              that._data.recordLog("success", "Season has been re-made successfully!").then(() => {
                that.generating = false;
                that.messageService.add({key:"primary", severity:'success', summary: "Season Re-Generated", detail: "Season has been re-made successfully!"});
                that.loading = false;
                that._router.navigate(['games/show-all-games'])
              })
              });
          }
        })
      } else {
        that.loading = false;
      }
    })
  }

  getMonday(date) {
    date = new Date(date);
    var day = date.getDay(),
        diff = date.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(date.setDate(diff));
  }

  fillTeamLists(teams: any) {
    let that = this;
    for(let currentTeam in teams) {
      let teamAvaliablilityDay = teams[currentTeam].avaliability.day;
      switch (teamAvaliablilityDay) {
        case "monday":
          if(teams[currentTeam].grade == "bondi") {
            that.teams.monday.bondi.push(teams[currentTeam]);
          } else {
            that.teams.monday.kokomo.push(teams[currentTeam]);
          }
          break;
        case "tuesday":
          if(teams[currentTeam].grade == "bondi") {
            that.teams.tuesday.bondi.push(teams[currentTeam]);
          } else {
            that.teams.tuesday.kokomo.push(teams[currentTeam]);
          }
          break;
        case "wednesday":
          if(teams[currentTeam].grade == "bondi") {
            that.teams.wednesday.bondi.push(teams[currentTeam]);
          } else {
            that.teams.wednesday.kokomo.push(teams[currentTeam]);
          }
          break;
      }
    }
  }

  // Fils the select object for every monday of the year
  fillMondaysList() {
    let d = new Date()
    let year = d.getFullYear()
    d = new Date("01/01/" + year);
    d.setDate(1);
    while (d.getDay() !== 1) {
        d.setDate(d.getDate() + 1);
    }
    while (d.getFullYear() === year) {
      var pushDate = new Date(d.getTime());
      this.mondays.push({day: (pushDate.getDate() < 10 ? '0' : '') + pushDate.getDate() + '/' + (pushDate.getMonth() < 9 ? '0' : '') + (pushDate.getMonth()+1) + '/' + pushDate.getFullYear()});
      d.setDate(d.getDate() + 7);
    }
  }

  // Displays what week of the season we are in
  getSeasonText(season) {
    let that = this;
    for(let i = 0; i < season.length; i++) {
      let usModifiedDate = that._data.convertDate(season[i].startDate);
      let today = new Date();
      if(i == 0) {
        if(today < new Date(usModifiedDate)) {
          that.currentWeek = "Season starts " + season[i].startDate;
          break;
        }
      }
      if(today >= new Date(usModifiedDate) && today <= that.addDays((new Date(usModifiedDate)), 7)) {
        that.currentWeek = "This is week " + (i+1) + " of " + season.length;
      }
    }
    if(this.currentWeek == "") {
      this.createNewSeason = true;
      that.currentWeek = "Season Finished, Please build new season.";
    }
  }

  addDays(dateObj, numDays) {
    var ms = dateObj.getTime() + (86400000 * numDays);
    var nextWeek = new Date(ms);
    return nextWeek;
  }

  generateSeasonForDay(teams) {
    let that = this;
    if (teams.length > 0) {
      if(teams.length % 2 == 1) {
        teams.unshift({team_name: "Fill"});
      }
      let numOfTeams = teams.length;
      let numOfRounds = numOfTeams / 2;
      let secondTeamIndex = 2;
      let seasonTemplate = [];
      for(let i = 0; i < numOfRounds; i++) {
        let roundTemplate = [];
        if(i == 0) {
          roundTemplate.unshift("static")
          roundTemplate.push(numOfTeams - secondTeamIndex);
        } else {
          roundTemplate.unshift(i - 1);
          roundTemplate.push(numOfTeams - secondTeamIndex);
        }
        secondTeamIndex += 1;
        seasonTemplate.push(roundTemplate)
      }
      let dynamicTeams = Array.from(teams);
      let staticTeam = dynamicTeams.shift();
      let season = [];
      for(let i = 0; i < that.seasonLength; i++) {
        let week = [];
        for(let round in seasonTemplate) {
          if(seasonTemplate[round][0] == "static") {
            week.push([staticTeam, dynamicTeams[seasonTemplate[round][1]], that._data.newUUID()])
          } else {
            week.push([dynamicTeams[seasonTemplate[round][0]], dynamicTeams[seasonTemplate[round][1]], that._data.newUUID()])
          }
        }
        let sessions = that._data.game_times.length;
        let weekSessions = week.length;
        if((sessions - weekSessions) > 0) {
          for(let y = 0; y < (sessions - weekSessions); y++) {
            week.push("empty" + y);
          }
        }
        season.push(week)
        let moveableObject = dynamicTeams.pop();
        dynamicTeams.unshift(moveableObject);
      }
      return season;
    } else {
      let season = [];
      for(let i = 0; i < that.seasonLength; i++) {
        let week = [];
        let sessions = that._data.game_times.length;
        for(let y = 0; y < sessions; y++) {
          week.push("empty" + y);
        }
        season.push(week);
      }
      return season;
    }
  }

  generateNewSeason() {
    let that = this;
    that.generating = true;
    let today = new Date();
    let monday = this.getMonday(today);
    let nextMonday = this.addDays(monday, 7);

    that.selectedStartDate = {day: (nextMonday.getDate() < 10 ? '0' : '') + nextMonday.getDate() + '/' + (nextMonday.getMonth() < 9 ? '0' : '') + (nextMonday.getMonth() + 1) + '/' + nextMonday.getFullYear()};
    let mondayKokomo = this.generateSeasonForDay(this.teams.monday.kokomo);
    let tuesdayKokomo = this.generateSeasonForDay(this.teams.tuesday.kokomo);
    let wednesdayKokomo = this.generateSeasonForDay(this.teams.wednesday.kokomo);
    let mondayBondi = this.generateSeasonForDay(this.teams.monday.bondi);
    let tuesdayBondi = this.generateSeasonForDay(this.teams.tuesday.bondi);
    let wednesdayBondi = this.generateSeasonForDay(this.teams.wednesday.bondi);
    that._data.saveSeason(mondayKokomo, mondayBondi, tuesdayKokomo, tuesdayBondi, wednesdayKokomo, wednesdayBondi, that.seasonLength, that.selectedStartDate).then(() => {
      that._data.recordLog("success", "New season has been generated successfully!").then(() => {
        that.generating = false;
        that.messageService.add({key:"primary", severity:'success', summary: "Season Created", detail: "Season has been made successfully!"});
        that.loading = false;
      })
    });
  }

  generateSeason() {
    let that = this;
    that.generating = true;
    let mondayKokomo = this.generateSeasonForDay(this.teams.monday.kokomo);
    let tuesdayKokomo = this.generateSeasonForDay(this.teams.tuesday.kokomo);
    let wednesdayKokomo = this.generateSeasonForDay(this.teams.wednesday.kokomo);
    let mondayBondi = this.generateSeasonForDay(this.teams.monday.bondi);
    let tuesdayBondi = this.generateSeasonForDay(this.teams.tuesday.bondi);
    let wednesdayBondi = this.generateSeasonForDay(this.teams.wednesday.bondi);
    that._data.saveSeason(mondayKokomo, mondayBondi, tuesdayKokomo, tuesdayBondi, wednesdayKokomo, wednesdayBondi, that.seasonLength, that.selectedStartDate).then(() => {
      that._data.recordLog("success", "Season has been created successfully!").then(() => {
        that.generating = false;
        that.messageService.add({key:"primary", severity:'success', summary: "Season Built", detail: "Season has been created successfully!"});
      });
    });
  }
}
