import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RebuildSeasonComponent } from './rebuild-season.component';

describe('RebuildSeasonComponent', () => {
  let component: RebuildSeasonComponent;
  let fixture: ComponentFixture<RebuildSeasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RebuildSeasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RebuildSeasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
