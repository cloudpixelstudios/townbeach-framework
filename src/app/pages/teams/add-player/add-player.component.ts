import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.scss']
})
export class AddPlayerComponent {

  //True if the player is being saved
  public loading = false;

  //True if player is being edited
  public isEdit = false;

  public player;

  constructor(private _data: DataService, private messageService: MessageService) {
    let that = this;
    //Set default player config
    this.resetPlayer();
    //Is the player being edited
    if(that._data.action == "edit_player") {
      that.player = that._data.actionData;
      that.isEdit = true;
      that._data.actionData = undefined;
      that._data.action = "default";
    }
  }

  //Reset player to default config
  resetPlayer() {
    this.player = {
      name: "",
      first_name: "",
      last_name: "",
      dob: "",
      type: "new",
      uid: this._data.newUUID()
    }
  }


  createPlayer() {
    let that = this;
    that.loading = true;
    //Check DOB and record invalid if its formatted badly
    let usDOB = that._data.convertDate(this.player.dob);
    if(Date.parse(usDOB).toString() == "NaN") {
      this.player.dob = "Invalid";
      that.messageService.add({key:"primary", severity:'info', summary: "Invalid DOB", detail: "Invalid date of birth - Setting default value"});
    }
    //Add player to RTDB
    this._data.addPlayers([this.player], this.isEdit).then(user => {
      that._data.recordLog("success", "Added player " + that.player.name + " successfully").then(() => {
        that.loading = false;
        that.messageService.add({key:"primary", severity:'success', summary: "Player Added", detail: that.player.name + " has been added successfully!"});
        that.resetPlayer();
      })
    })
  }

  //Realtime generation of full name from first + last
  compileName(player) {
    player["name"] = player["first_name"] + " " + player["last_name"];
  }

  //DOB masking on every keystroke
  KeyUpCalled(){
    this.player.dob = this._data.maskDate(this.player.dob);
  }

}
