import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-remove-player',
  templateUrl: './remove-player.component.html',
  styleUrls: ['./remove-player.component.scss']
})
export class RemovePlayerComponent {

  // True if loading players
  public loading = true;

  // True if deleting the player
  public deleting = false;

  // Public observable of players from RTDB
  private playersSubject = new BehaviorSubject(null);
  public players = this.playersSubject.asObservable();

  // Player to delete
  public selectedPlayer;

  constructor(private _data: DataService, private _router: Router, private messageService: MessageService) {
    let that = this;
    that._data.players.subscribe(players => {
      that.playersSubject.next(players);
      that.loading = false;
    })
  }

  removePlayer() {
    let that = this;
    that.deleting = true;
    if(that.selectedPlayer) {
      // Remove player and record log
      that._data.removePlayer(that.selectedPlayer.uid).then(() => {
        that._data.recordLog("info", that.selectedPlayer.name + ' has now been removed from active games and the player list.').then(() => {
          that.messageService.add({severity:'success', summary:'Team Removed', detail: that.selectedPlayer.name + ' has now been removed from active games', key: 'primary'});
          that.deleting = false;
        })
      })
    }
  }

}
