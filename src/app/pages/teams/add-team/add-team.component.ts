import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../../services/data.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { TabView } from 'primeng/tabview';
import { AutoComplete } from 'primeng/AutoComplete';

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.scss']
})
export class AddTeamComponent {

  // Selected captain of team
  public captain;

  // Team object
  public team;

  // Array of player objects
  public players = [];

  // Pick captain dialgo displayed if true
  private captainDisplay: boolean = false;

  // True while saving team
  private loading = false;

  // List of existing players filtered by search
  public suggestedPlayers;

  // True if the team is in edit mode
  private isEdit: boolean = false;

  constructor(private _data: DataService, private messageService: MessageService, private _router: Router) {
    let that = this;
    // Set team defaults
    this.resetTeam();

    // Get the players from RTDB once and use to fill initial suggested existing players list
    that._data.players.pipe(take(1)).subscribe(players => {
      that.suggestedPlayers = players;
      // If the team is an edit, add all players of team to ui for manipulation
      if (that._data.action == "edit_team") {
        that.team = that._data.actionData;
        that.isEdit = true;
        that.players = that.team.players;
        that.team.players = [];
        for(let i = 0; i < that.players.length; i++ ) {
          that.players[i].type = "picked";
          for(let j = 0; j < that.suggestedPlayers.length; j++) {
            if(that.suggestedPlayers[j].uid == that.players[i].uid) {
              this.suggestedPlayers.splice(j, 1);
            }
          }
        }
        that._data.action = "default";
        that._data.actionData = undefined;
      }
    });
  }

  // Real time compiler for name, made from First + Last name
  compileName(player) {
    player["name"] = player["first_name"] + " " + player["last_name"];
  }

  // Add player to the players list, make captain if this is the first player in team
  addPlayer() {
    let uuid = this._data.newUUID();
    if(this.players.length == 0) {
      this.players.push({first_name:"", last_name:"", name:"", dob:"", uid: uuid, type: "new"})
      this.team.captain = uuid;
    } else {
      this.players.push({first_name:"", last_name:"", name:"", dob:"", uid: uuid, type: "new"})
    }
  }

  // Add the existing player to the players list, make captain if this is the first player in team
  pickPlayer(player, i) {
    player["type"] = "picked";
    this.players[i] = player;
    if(i == 0) {
      this.team.captain = player.uid
    }
    for(let i = 0; i < this.suggestedPlayers.length; i++) {
      for(let j = 0; j< this.players.length; j++) {
        if(this.suggestedPlayers[i].uid == this.players[j].uid) {
          this.suggestedPlayers.splice(i, 1);
        }
      }
    }
  }

  // Show the existing players dropdown if there are players left that arent on team
  addExistingPlayer() {
    if(this.suggestedPlayers.length == 0) {
      this.messageService.add({key:"primary", severity:'error', summary: "Invalid Task", detail: "No players to add that are not already added"});
    } else {
      let found = false;
      for(let i = 0; i < this.players.length; i++) {
        if (this.players[i].type == "existing") {
          found = true;
        }
      }
      if(found) {
        this.messageService.add({key:"primary", severity:'error', summary: "Invalid Task", detail: "Please finish adding current players"});
      } else {
        // Add player with type existing to show dropdown box
        this.players.push({first_name:"", last_name:"", name:"", dob:"", uid: "demo", type: "existing"});
      }
    }
  }

  // Change captain of the team
  changeCaptain() {
    for(let i = 0; i < this.players.length; i++) {
      if(this.players[i] == this.captain) {
        this.team.captain = this.players[i]["uid"];
      }
    }
  }

  // Show captain modal dropdown
  selectCaptain() {
    if (this.team.captain == "") {
      this.team.captain = this.players[0].uid;
    }
    this.captainDisplay = true;
  }

  // Close Captain Modal
  closeCaptain() {
    this.captainDisplay = false;
  }

  // Generate team and upload to RTDB
  generateTeam() {
    let that = this;
    // Loading spinner activated
    that.loading = true;
    // Players uid array to be stored in team object
    const uploadTeamPlayers = [];
    for(let i = 0; i < that.players.length; i++) {
      that.players[i].name = that.players[i].first_name + " " + that.players[i].last_name;
      uploadTeamPlayers.push(that.players[i]["uid"]);
      // Format date - make invalid if date is not supported
      let usDOB = that._data.convertDate(that.players[i].dob);
      if(Date.parse(usDOB).toString() == "NaN") {
        that.players[i].dob = "Invalid";
        that.messageService.add({key:"primary", severity:'info', summary: "Invalid DOB", detail: "Invalid date of birth - Setting default value"});
      }
      // Add players to RTDB
      if(i == that.players.length - 1) {
        that._data.addPlayers(that.players, false).then(() => {
          that.team.players = uploadTeamPlayers;
          // Add team to RTDB
          that._data.addTeam(that.team, that.isEdit).then(result => {
            that.loading = false;
            that.messageService.add({key:"primary", severity:'success', summary: "Team Added", detail: that.team.team_name + " has been added successfully!"});
            that.messageService.add({key:"primary", severity:'info', summary: "Rebuild Season", detail: "Please rebuild the season so \'" + that.team.team_name + "\' can be included."});
            that.resetTeam();
            if(that.isEdit){
              that._data.recordLog("success", that.team.team_name + " edits have been saved successfully!").then(() => {
                that._router.navigate(["teams/show-all-teams"])
              });
            } else {
              that._data.recordLog("success", that.team.team_name + " edits have been saved successfully!").then(() => {
                that._data.action = "rebuild_season";
                that._router.navigate(["games/current-season"]);
              });
            }
          });
        })
      }
    }
  }

  // Remove player from players array
  removePlayer(i) {
    if(this.players[i].type == "picked") {
      this.suggestedPlayers.push(this.players[i]);
    }
    this.players.splice(i, 1);
  }

  // Reset team to default values
  resetTeam() {
    this.team = {
      team_name: "",
      players: new Array<any>(),
      grade:"bondi",
      avaliability: {
        times: [],
        day: ""
      },
      captain: "",
      points: 0,
      uid: "",
      notes: "",
      status: "active"
    }
    this.players = [];
  }

  // Mask dob when adding new player, called on every keystroke in DOB field
  KeyUpCalled(i){
    this.players[i].dob = this._data.maskDate(this.players[i].dob);
  }

}
