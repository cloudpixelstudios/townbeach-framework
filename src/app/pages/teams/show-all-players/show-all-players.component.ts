import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-all-players',
  templateUrl: './show-all-players.component.html',
  styleUrls: ['./show-all-players.component.scss']
})
export class ShowAllPlayersComponent {

  // Public obserable for players from RTDB
  private playersSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public players: Observable<any> = this.playersSubject.asObservable();

  // Static players list
  public playersStatic = [];

  // Filtered players list from text input
  public playersFilter = [];

  constructor(private _data: DataService, private _router: Router) {
    let that = this;
    that._data.players.subscribe(players => {
      that.playersSubject.next(players);
      that.playersStatic = players;
      that.playersFilter = players;
    })
  }

  // Filter names based on text input
  filter(nameString) {
    let that = this;
    that.playersFilter = [];
    for(let i = 0; i < that.playersStatic.length; i++) {
      if(that.playersStatic[i].name.toLowerCase().includes(nameString)) {
        that.playersFilter.push(this.playersStatic[i]);
      }
    }
  }

  // Edit player details
  public editPlayer(player) {
    this._data.action = "edit_player";
    this._data.actionData = player;
    this._router.navigate(["/teams/add-new/add-player"]);
  }

}
