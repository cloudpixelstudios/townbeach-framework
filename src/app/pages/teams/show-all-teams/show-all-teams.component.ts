import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-all-teams',
  templateUrl: './show-all-teams.component.html',
  styleUrls: ['./show-all-teams.component.scss']
})
export class ShowAllTeamsComponent {

  // Public observable of the teams from RTDB
  private teamsSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public teams: Observable<any> = this.teamsSubject.asObservable();
  // Public observable of the players from RTDB
  private playersSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public players: Observable<any> = this.playersSubject.asObservable();

  // Static teams list
  public staticTeams = [];
  // Static bondi and kokomo teams list
  public bondiTeams = [];
  public kokomoTeams = [];

  // True if loading RTDB data
  public loading: boolean = true;

  constructor(private _data: DataService, private _router: Router) {
    let that = this;
    that._data.teams.subscribe(teams => {
      let teamsList = [];
      for(const team in teams) {
        teamsList.push(teams[team]);
      }
      if (teamsList.length == 0) {
        that.staticTeams = [];
        that.loading = false;
      } else {
        that.teamsSubject.next(teamsList);
        that._data.players.subscribe(players => {
          let playersList = [];
          for(const player in players) {
            playersList.push(players[player]);
          }
          that.bondiTeams = [];
          that.kokomoTeams = [];
          that.staticTeams = [];
          that.playersSubject.next(playersList);
          for(let i = 0; i < teamsList.length; i++) {
            let tempTeam = teamsList[i];
            if(tempTeam.grade == "bondi") {
              this.bondiTeams.push(tempTeam);
            } else {
              this.kokomoTeams.push(tempTeam);
            }
            let tempPlayers = [];
            for(let j = 0; j < teamsList[i].players.length; j++) {
              for(let k = 0; k < playersList.length; k++) {
                if(playersList[k].uid == teamsList[i].players[j]) {
                  tempPlayers.push(playersList[k]);
                }
              }
            }
            teamsList[i].players = tempPlayers;
            that.staticTeams.push(tempTeam);
            that.loading = false;
          }
        })
      }
    });
  }

  // Edit player on team
  public editPlayer(player) {
    this._data.action = "edit_player";
    this._data.actionData = player;
    this._router.navigate(["/teams/add-new/add-player"]);
  }

  // Edit team
  public editTeam(team) {
    this._data.action = "edit_team";
    this._data.actionData = team;
    this._router.navigate(["/teams/add-new/add-team"]);
  }

  // Get user data from uid
  public getTeamPlayers(uid) {
    let that = this;
    for(let i = 0; i < this.staticTeams.length; i++) {
      if(this.staticTeams[i].uid == uid){
        return this.staticTeams[i].players;
      }
    }
  }

  // Define age from DOB
  public getAge(dob) {
    if(dob != "Invalid"){
      var numbers = dob.match(/\d+/g);
      var dobString = new Date(numbers[2], numbers[1]-1, numbers[0]);
      var today = new Date();
      var birthDate = new Date(dobString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
      {
          age--;
      }
      return age;
    } else {
      return 0;
    }
  }
}
