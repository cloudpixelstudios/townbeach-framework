import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisqualifyTeamComponent } from './disqualify-team.component';

describe('DisqualifyTeamComponent', () => {
  let component: DisqualifyTeamComponent;
  let fixture: ComponentFixture<DisqualifyTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisqualifyTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisqualifyTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
