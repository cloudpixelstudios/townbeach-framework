import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { MessageService } from 'primeng/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-disqualify-team',
  templateUrl: './disqualify-team.component.html',
  styleUrls: ['./disqualify-team.component.scss']
})
export class DisqualifyTeamComponent implements OnInit {
  // Team to delete
  private selectedTeam = undefined;

  // Public observable of teams from RTDB
  private teamsSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public teams: Observable<any> = this.teamsSubject.asObservable();

  // True if removing team
  private loading = false;

  constructor(private _data: DataService, private messageService: MessageService, private _router: Router) {
    this._data.teams.subscribe(teams => {
      this.teamsSubject.next(teams);
      this.selectedTeam = teams[0];
    })
  }

  ngOnInit() {
  }

  private removeTeam() {
    let that = this;
    that.loading = true;
    // Remove team and record a log, send to rebuild season
    that._data.removeTeam(that.selectedTeam).then(() => {
      that._data.recordLog("info", that.selectedTeam.team_name + ' has now been removed. Season Needs Rebuilding.').then(() => {
        that.messageService.add({severity:'success', summary:'Team Removed', detail: that.selectedTeam.team_name + ' has now been removed.', key: 'primary'});
        that.messageService.add({key:"primary", severity:'info', summary: "Rebuild Season", detail: "Please rebuild the season so \'" + that.selectedTeam.team_name + "\' can be excluded."});
        that.loading = false;
        that._data.action = "rebuild_season";
        that._router.navigate(["games/current-season"]);
      })
    })
  }
}
