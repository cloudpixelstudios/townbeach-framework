import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

  //Internal teams subject - exposed publicly as an observable
  private completedSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public completed: Observable<any> = this.completedSubject.asObservable();
  public completedStatic = [];

  public teams = [];

  //Static Bondi and Kokomo Teams
  public tempTeams = [];
  public tempTeamsPoints = [];

  public cancelledTeams = [];
  public cancelledTeamsPoints = []
  public bondiTeams = [];
  public bondiTeamsPoints = [];
  public kokomoTeams = [];
  public kokomoTeamsPoints = [];

  getTeamname(uid) {
    for(let i = 0; i < this.teams.length; i++) {
      if(this.teams[i].uid == uid) {
        return this.teams[i].team_name
      }
    }
    for(let i = 0; i < this.completedStatic.length; i++) {
      if (this.completedStatic[i].teamOne.uid == uid) {
        return this.completedStatic[i].teamOne.team_name
      }
      if (this.completedStatic[i].teamTwo.uid == uid) {
        return this.completedStatic[i].teamTwo.team_name
      }
    }
    return "";
  }

  constructor(private _data: DataService) {
    let that = this;
    //Get teams object from cloud data and push to each static array

    that._data.teams.pipe(take(1)).subscribe(teams => {
      that.teams = teams;
      that._data.completed.pipe(take(1)).subscribe(completed => {
        this.completedStatic = completed;
        console.log(completed)
        that.tempTeams = [];
        that.tempTeamsPoints = [];
        //Assign teams object to local teams subject
        that.completedSubject.next(completed);
        for(let i = 0; i < completed.length; i++) {
          if(this.tempTeams.includes(completed[i].teamOne.uid)) {
            let indexOfTeam = this.tempTeams.indexOf(completed[i].teamOne.uid)
            this.tempTeamsPoints[indexOfTeam] += this.getScore(completed[i].teamOne, completed[i].teamTwo);
          } else {
            this.tempTeams.push(completed[i].teamOne.uid)
            this.tempTeamsPoints.push(this.getScore(completed[i].teamOne, completed[i].teamTwo));
          }
          if(this.tempTeams.includes(completed[i].teamTwo.uid)) {
            let indexOfTeam = this.tempTeams.indexOf(completed[i].teamTwo.uid)
            this.tempTeamsPoints[indexOfTeam] += this.getScore(completed[i].teamTwo, completed[i].teamOne);
          } else {
            this.tempTeams.push(completed[i].teamTwo.uid)
            this.tempTeamsPoints.push(this.getScore(completed[i].teamTwo, completed[i].teamOne));
          }
        }
        for(let i = 0; i < this.tempTeams.length; i++) {
          let found = false;
          for(let j = 0; j < this.teams.length; j++) {
            if(this.teams[j].uid == this.tempTeams[i]) {
              if(this.teams[j].grade == "bondi") {
                this.bondiTeams.push(this.tempTeams[i]);
                this.bondiTeamsPoints.push(this.tempTeamsPoints[i])
                found = true;
              } else if(this.teams[j].grade == "kokomo") {
                this.kokomoTeams.push(this.tempTeams[i]);
                this.kokomoTeamsPoints.push(this.tempTeamsPoints[i])
                found = true;
              }
            }
          }
          if (!found) {
            this.cancelledTeams.push(this.tempTeams[i]);
            this.cancelledTeamsPoints.push(this.tempTeamsPoints[i])
          }
        }
      });
    });
  }

  getScore(teamx, teamy) {
    let teamxPoints = 0;
    let teamyPoints = 0;
    if(teamx.uid == "xx1" || teamx.uid == "xx2") {
      return 0;
    } else if(teamy.uid == "xx1" || teamy.uid == "xx2") {
      teamxPoints += 10;
    } else {
      if(teamx.score > teamy.score) {
        teamxPoints += 10;
        teamyPoints += 5;
      } else if (teamx.score < teamy.score) {
        teamyPoints += 10;
        teamxPoints += 5;
      } else {
        teamyPoints += 7;
        teamxPoints += 7;
      }
    }
    teamxPoints = teamxPoints + this.getExtraPoints(teamx.score);
    teamyPoints = teamyPoints + this.getExtraPoints(teamy.score);
    return teamxPoints;
  }

  getExtraPoints(teamScore) {
   if (teamScore >= 10 && teamScore < 20) {
     return 1;
   } else if (teamScore >= 20 && teamScore < 30) {
     return 2;
   } else if (teamScore >= 30 && teamScore < 40) {
     return 3;
   } else if (teamScore >= 40 && teamScore < 50) {
     return 4;
   } else if (teamScore >= 50 && teamScore < 60) {
     return 5;
   } else if (teamScore >= 60 && teamScore < 70) {
     return 6;
   } else if (teamScore >= 70 && teamScore < 80) {
     return 7;
   } else if (teamScore >= 80) {
     return 8;
   } else {
     return 0;
   }
 }


  ngOnInit() {
  }

}
