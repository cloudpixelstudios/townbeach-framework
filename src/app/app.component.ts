import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { DataService } from './services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MessageService]
})
export class AppComponent implements OnInit {
  items: MenuItem[];

  constructor(private messageService: MessageService, private _data: DataService, private _router: Router){
  }

  logout() {
    let that = this;
    // Show login after logout
    that._data.logout().then(() => {
      that._router.navigate(['login'])
    });
  }

  ngOnInit() {
    // Menu setup
    this.items = [
        {
            label: 'Teams',
            icon: 'pi pi-fw pi-users',
            items: [
              {
                label: 'Add New',
                icon: 'pi pi-fw pi-plus',
                items: [
                    {label: 'Team', routerLink: 'teams/add-new/add-team', icon: 'pi pi-fw pi-users'},
                    {label: 'Player', routerLink: 'teams/add-new/add-player', icon: 'pi pi-fw pi-user'},
                ]
              },
              {separator:true},
              //{label: 'Payments', routerLink: 'teams/paying-players', icon: 'pi pi-fw pi-money-bill'},
              {label: 'Show All Players', routerLink: 'teams/show-all-players', icon: 'pi pi-fw pi-users'},
              {label: 'Show All Teams', routerLink: 'teams/show-all-teams', icon: 'pi pi-fw pi-list'},
              {separator:true},
              {
                label: 'Remove',
                icon: 'pi pi-fw pi-minus',
                items: [
                    {label: 'Team', routerLink: 'teams/remove-team', icon: 'pi pi-fw pi-users'},
                    {label: 'Player', routerLink: 'teams/remove-player', icon: 'pi pi-fw pi-user'},
                ]
              }
            ]
        },
        {
            label: 'Games',
            icon: 'pi pi-fw pi-file',
            items: [
                {label: 'Current Season', icon: 'pi pi-fw pi-refresh', routerLink: 'games/current-season'},
                {label: 'Show All Games', routerLink: 'games/show-all-games', icon: 'pi pi-fw pi-list'},
            ]
        },
        {
            label: 'Leaderboard',
            routerLink: 'leaderboard',
            icon: 'pi pi-fw pi-chart-bar'
        }
    ];
  }
}
