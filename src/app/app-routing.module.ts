import { NgModule } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { Routes, RouterModule } from '@angular/router';
import { AddTeamComponent } from './pages/teams/add-team/add-team.component';
import { AddPlayerComponent } from './pages/teams/add-player/add-player.component';
import { ShowAllTeamsComponent } from './pages/teams/show-all-teams/show-all-teams.component';
import { ShowAllPlayersComponent } from './pages/teams/show-all-players/show-all-players.component';
import { ShowAllGamesComponent } from './pages/games/show-all-games/show-all-games.component';
import { CurrentSeasonComponent } from './pages/games/current-season/current-season.component';
import { LeaderboardComponent } from './pages/leaderboard/leaderboard.component';
import { DisqualifyTeamComponent } from './pages/teams/disqualify-team/disqualify-team.component';
import { RemovePlayerComponent } from './pages/teams/remove-player/remove-player.component';
import { LoginComponent } from './pages/login/login/login.component';
import { LogsComponent } from './pages/logs/logs.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: 'teams', children: [
    { path: 'add-new', children:[
      { path: 'add-team', component: AddTeamComponent },
      { path: 'add-player', component: AddPlayerComponent }
    ], canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
    { path: 'show-all-players', component: ShowAllPlayersComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } },
    { path: 'show-all-teams', component: ShowAllTeamsComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } },
    { path: 'remove-team', component: DisqualifyTeamComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } },
    { path: 'remove-player', component: RemovePlayerComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } }
  ]},
  { path: 'games', children: [
    { path: 'show-all-games', component: ShowAllGamesComponent },
    { path: 'current-season', component: CurrentSeasonComponent }
  ], canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'leaderboard', component: LeaderboardComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'login', component: LoginComponent},
  { path: 'logs', component: LogsComponent},
  { path: '',   redirectTo: '/games/show-all-games', pathMatch: 'full', canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
