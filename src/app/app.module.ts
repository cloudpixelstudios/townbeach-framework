import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DataService } from './services/data.service';

import { MenubarModule } from 'primeng/menubar';
import { CardModule } from 'primeng/card';
import { DataViewModule } from 'primeng/dataview';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { AutoCompleteModule } from 'primeng/AutoComplete';
import { OrderListModule } from 'primeng/orderlist';
import { CheckboxModule } from 'primeng/checkbox';

import { AddTeamComponent } from './pages/teams/add-team/add-team.component';
import { AddPlayerComponent } from './pages/teams/add-player/add-player.component';
import { ShowAllTeamsComponent } from './pages/teams/show-all-teams/show-all-teams.component';
import { ShowAllGamesComponent } from './pages/games/show-all-games/show-all-games.component';
import { CurrentSeasonComponent } from './pages/games/current-season/current-season.component';
import { LeaderboardComponent } from './pages/leaderboard/leaderboard.component';
import { DisqualifyTeamComponent } from './pages/teams/disqualify-team/disqualify-team.component';
import { SpinnerComponent } from './utility/spinner/spinner.component';
import { ShowAllPlayersComponent } from './pages/teams/show-all-players/show-all-players.component';
import { RemovePlayerComponent } from './pages/teams/remove-player/remove-player.component';
import { LoginComponent } from './pages/login/login/login.component';
import { LogsComponent } from './pages/logs/logs.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NoCacheHeadersInterceptor } from './services/interceptor';

export function configLoad(_data: DataService) {
    return () => _data.bootstrapApp();
}

@NgModule({
  declarations: [
    AppComponent,
    AddTeamComponent,
    AddPlayerComponent,
    ShowAllTeamsComponent,
    ShowAllGamesComponent,
    CurrentSeasonComponent,
    LeaderboardComponent,
    DisqualifyTeamComponent,
    SpinnerComponent,
    ShowAllPlayersComponent,
    RemovePlayerComponent,
    LoginComponent,
    LogsComponent
  ],
  imports: [
    BrowserModule,
    MenubarModule,
    CardModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DataViewModule,
    AccordionModule,
    TableModule,
    InputTextModule,
    ButtonModule,
    DropdownModule,
    FormsModule,
    DialogModule,
    SelectButtonModule,
    InputTextareaModule,
    ToastModule,
    OrderListModule,
    TabViewModule,
    CheckboxModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireAuthGuardModule,
    AngularFireDatabaseModule,
    AutoCompleteModule
  ],
  providers: [ MessageService, DataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NoCacheHeadersInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: configLoad,
      deps: [DataService],
      multi: true
    } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
