import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //Constant Days Played
  public game_days = [
    {label: "Mon", value: "monday"},
    {label: "Tues", value: "tuesday"},
    {label: "Wed", value: "wednesday"}
  ];

  //Constant Game PLaying Times
  public game_times = [
    {label: "6:00", value: "1800"},
    {label: "6:40", value: "1840"},
    {label: "7:20", value: "1920"},
    {label: "8:00", value: "2000"},
    {label: "8:40", value: "2040"}
  ];

  //Court Sides
  public game_grades = [
    {label: "Kokomo", value: "kokomo"},
    {label: "Bondi", value: "bondi"}
  ];

  //Custom actions when moving betwen controllers
  public action: string = "default";
  public actionData;

  //User login state
  public userState = this._auth.user;

  // Completed games object - Updates on change to RTDB
  public completed: Observable<any> = this._database.list('completed').valueChanges();

  //All teams object - Updates on change to RTDB
  public teams: Observable<any> = this._database.list('teams').valueChanges();

  //All players object - Updates on change to RTDB
  public players: Observable<any> = this._database.list('players').valueChanges();

  //All ganes object - Updates on change to RTDB
  public seasons: Observable<any> = this._database.list('season').valueChanges();

  //All logs object - Updates on change to RTDB
  public logs: Observable<any> = this._database.list('logs', ref => ref.limitToLast(25)).valueChanges();

  constructor(private _database: AngularFireDatabase, private _auth: AngularFireAuth) {}

  // Records activity to the RTDB
  public recordLog(status, message) {
    let that = this;
    return new Promise((resolve, reject) => {
      let time = new Date().toString();
      that._database.list('logs').push({message: message, time: time, status: status}).then(() => {
        resolve(true);
      })
    })
  }

  //Logs out current user and forces AuthGuard to login page
  public logout() {
    let that = this;
    return new Promise((resolve, reject) => {
      this._auth.auth.signOut().then(() => {
        resolve(true);
      });
    });
  }

  //Login user, AuthGuard now accepts the authenticated user -> sends to default route
  public login(credentials) {
    let that = this;
    return new Promise((resolve, reject) => {
      that._auth.auth.signInWithEmailAndPassword(credentials.username, credentials.password).then(user => {
        resolve(user);
      }).catch(error => {
        reject(error);
      })
    })
  }

  addDays(dateObj, numDays) {
   dateObj.setDate(dateObj.getDate() + numDays);
   return dateObj;
  }

  // Preload RTDB sync
  public bootstrapApp() {
    return new Promise((resolve, reject) => {
      resolve(true);
    })
  }

  //Save a given season to RTDB
  public saveSeason(mondayKokomo, mondayBondi, tuesdayKokomo, tuesdayBondi, wednesdayKokomo, wednesdayBondi, seasonLength, startDate) {
    let that = this;
    return new Promise((resolve, reject) => {
      let season = [];
      let currentStartDate = startDate.day;
      console.log(seasonLength)
      console.log(currentStartDate)
      for(let i = 0; i < seasonLength; i++) {
        if(i != 0) {
          let d = new Date(that.convertDate(currentStartDate));
          let modified = that.addDays(d, 7);
          currentStartDate = (modified.getDate() < 10 ? '0' : '') + modified.getDate() + '/' + (modified.getMonth() < 9 ? '0' : '') + (modified.getMonth() + 1) + '/' + modified.getFullYear()
        }
        let currentWeek = { mondayKokomo: [], tuesdayKokomo: [], wednesdayKokomo: [], mondayBondi: [], tuesdayBondi: [], wednesdayBondi: [], startDate: currentStartDate };
        if(mondayBondi) {
          currentWeek.mondayBondi = mondayBondi[i];
        } else {
          currentWeek.mondayBondi = [];
        }
        if(tuesdayBondi) {
          currentWeek.tuesdayBondi = tuesdayBondi[i];
        } else {
          currentWeek.tuesdayBondi = [];
        }
        if(wednesdayBondi) {
          currentWeek.wednesdayBondi = wednesdayBondi[i];
        } else {
          currentWeek.wednesdayBondi = [];
        }
        if(mondayKokomo) {
          currentWeek.mondayKokomo = mondayKokomo[i];
        } else {
          currentWeek.mondayKokomo = [];
        }
        if(tuesdayKokomo) {
          currentWeek.tuesdayKokomo = tuesdayKokomo[i];
        } else {
          currentWeek.tuesdayKokomo = [];
        }
        if(wednesdayKokomo) {
          currentWeek.wednesdayKokomo = wednesdayKokomo[i];
        } else {
          currentWeek.wednesdayKokomo = [];
        }
        season.push(currentWeek);
        if(i == (seasonLength - 1)) {
          that._database.object('season').set(season).then(() => {
            resolve(true);
          })
        }
      }
    });
  }

  // Saves the season when changed in 'show all games'
  public saveAdjustedSeason(season) {
    let that = this;
    return new Promise((resolve, reject) => {
      that._database.object('season').set(season).then(() => {
        resolve(true);
      })
    });
  }

  // Convert between AU and US date format anf vice-versa. eg. 26/08/1995 => 08/26/1995. Switches 1st and second group of numbers around.
  public convertDate(oldDate) {
    return (oldDate[3] + oldDate[4] + "/" + oldDate[0] + oldDate[1] + "/" + oldDate[6] + oldDate[7] + oldDate[8] + oldDate[9]);
  }

  // Remove player and any team they are in, ensuring to reassign captain if it was them.
  public removePlayer(uid) {
    return new Promise((resolve, reject) => {
      let that = this;
      let teamsWithPlayer = [];
      that.teams.pipe(take(1)).subscribe(teams => {
        for(let i in teams) {
          for(let j in teams[i].players) {
            if(teams[i].players[j] == uid) {
              let removedPlayer = teams[i].players.splice(j, 1);
              teamsWithPlayer.push(teams[i]);
              if (removedPlayer == teams[i].captain) {
                teams[i].captain = teams[i].players[0];
              }
            }
          }
        }
        let doneCount = 0;
        if(teamsWithPlayer.length > 0) {
          for(let x = 0; x < teamsWithPlayer.length; x++) {
            that._database.object("teams/" + teamsWithPlayer[x].uid).set(teamsWithPlayer[x]).then(() => {
              doneCount += 1;
              if(doneCount == teamsWithPlayer.length) {
                that._database.object("players/" + uid).remove().then(() => {
                  resolve(true);
                })
              }
            })
          }
        } else {
          that._database.object("players/" + uid).remove().then(() => {
            resolve(true);
          })
        }
      });
    });
  }

  // Remove team from app, follow this with a rebuild season command.
  public removeTeam(key) {
    return new Promise((resolve, reject) => {
      let teamRef = this._database.list("teams");
      teamRef.remove(key.uid).then(() => {
        resolve(true);
      })
    });
  }

  // Add or edit player info, saving to RTDB
  public addPlayers(players, isEdit) {
    let that = this;
    return new Promise((resolve, reject) => {
      if(isEdit) {
        let playersRef = that._database.object('players/' + players[0].uid);
        playersRef.set(players[0]).then(() => {resolve(true)})
      } else {
        let playersUploaded = 0;
        for(let i = 0; i < players.length; i++) {
          if(players[i].type == "new") {
            delete players[i].type;
            let playersRef = that._database.object('players/' + players[i].uid);
            playersRef.set(players[i]).then(() => {
              playersUploaded += 1;
              if (playersUploaded == players.length) {
                resolve(true)
              }
            });
          } else {
            playersUploaded += 1;
            if (playersUploaded == players.length) {
              resolve(true)
            }
          }
        }
      }
    });
  }

  //Add team to the app - should follow ub with a rebuild command
  public addTeam(team, isEdit) {
    let that = this;
    if (!isEdit) {
      // Give team a unique custom identifier if its a new team
      team.uid = this.newUUID();
    }
    // Upload team to RTDB and resolve the team object
    return new Promise((resolve, reject) => {
      const teamsRef = that._database.object('teams/' + team.uid);
      teamsRef.set(team).then(() => {
        resolve(team)
      });
    });
  }

  //Masking for date to add slashes
  public maskDate(input) {
      var v = input;
      if (v.match(/^\d{2}$/) !== null) {
          input = v + '/';
      } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
          input = v + '/';
      }
      return input;
  }

  //Genrate a new UUID
  public newUUID() {
    //Timestamp
    var d = new Date().getTime();
    //Time in microseconds since page-load or 0 if unsupported
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      //random number between 0 and 16
        var r = Math.random() * 16;
        //Use timestamp until depleted
        if(d > 0){
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        //Use microseconds since page-load if supported
        } else {
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}
