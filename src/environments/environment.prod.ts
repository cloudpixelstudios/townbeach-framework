export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCbm5DaO9VBcRu7jK7H284GvDwnKbgHb88",
    authDomain: "townbeach-9225d.firebaseapp.com",
    databaseURL: "https://townbeach-9225d.firebaseio.com",
    projectId: "townbeach-9225d",
    storageBucket: "townbeach-9225d.appspot.com",
    messagingSenderId: "185379371037",
    appId: "1:185379371037:web:38459a053801cf11e5aef4",
    measurementId: "G-R40FRTTHNR"
  }
};
